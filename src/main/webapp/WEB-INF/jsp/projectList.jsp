<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Михаил
  Date: 028 28.10.20
  Time: 16:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="UTF-8"/>
    <title>Project List</title>
</head>
<body>
<h1>Project List</h1>

<br/><br/>
<div>
    <table border="1">
        <tr>
            <th>Name</th>
            <th>Description</th>
            <th>Status</th>
        </tr>
        <c:forEach items="${projects}" var="project">
            <tr>
                <td>${project.name}</td>
                <td>${project.description}</td>
                <td>${project.taskProjectStatus}</td>
                <td><a href='<c:url value="/project/${project.id}"/>'><c:url value="Read Project"/></a></td>
                <td><a href='<c:url value="/projectUpdate" />'><c:url value="Edit Project"/></a></td>
                <td><a href='<c:url value="/projectDelete" />'><c:url value="Delete Project"/></a></td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>
