<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Михаил
  Date: 002 02.11.20
  Time: 14:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Project</title>
</head>
<body>
<h1>Project</h1>
<table border="1">
    <tr>
        <th>Project Name</th>
        <th>Project Description</th>
        <th>Start Date</th>
        <th>Finish Date</th>
    </tr>
    <c:forEach items="${projects}" var="project">
        <tr>
            <td>${project.name}</td>
            <td>${project.description}</td>
            <td>${project.dataStart}</td>
            <td>${project.dataFinish}</td>
        </tr>
    </c:forEach>
</table>

<h1>Tasks</h1>
<table border="2">
    <tr>
        <th>Task Name</th>
        <th>TP Status</th>
        <th>Task Description</th>
        <th>Task Id</th>
        <th>Start Date</th>
        <th>Finish Date</th>
        <th>Create Date</th>
    </tr>
    <c:forEach items="${taskLists}" var="task">
        <tr>
            <td>${task.name}</td>
            <td>${task.taskProjectStatus}</td>
            <td>${task.description}</td>
            <td>${task.id}</td>
            <td>${task.dataStart}</td>
            <td>${task.dataFinish}</td>
            <td>${task.dataCreate}</td>
            <td><a href='<c:url value="/taskUpdate" />'><c:url value="Edit Task"/></a></td>
            <td><a href='<c:url value="/taskDelete" />'><c:url value="Delete Task"/></a></td>
        </tr>
    </c:forEach>
    <th>Tasks</th>

</table>
</body>
</html>
