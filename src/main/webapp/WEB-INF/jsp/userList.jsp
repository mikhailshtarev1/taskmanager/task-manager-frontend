<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Михаил
  Date: 022 22.10.20
  Time: 16:16
  To change this template use File | Settings | File Templates.
--%>
<%--<%@ page contentType="text/html;charset=UTF-8" language="java" %>--%>
<html>
<head>
    <meta charset="UTF-8" />
    <title>Person List</title>
    </head>
<body>
<h1>Person List</h1>

<br/><br/>
<div>
    <table border="1">
        <tr>
            <th>First Name</th>
            <th>User Id</th>
            <th>User Role</th>
        </tr>
        <c:forEach  items="${users}" var ="user">
            <tr>
                <td>${user.name}</td>
                <td>${user.userId}</td>
                <td>${user.userRole}</td>
            </tr>
        </c:forEach>
    </table>
</div>
</body>
</html>
