<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%--
  Created by IntelliJ IDEA.
  User: Михаил
  Date: 027 27.10.20
  Time: 10:53
  To change this template use File | Settings | File Templates.
--%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Create an account</title>

    <link href="${contextPath}/resources/css/bootstrap.min.css" rel="stylesheet">
    <link href="${contextPath}/resources/css/common.css" rel="stylesheet">
</head>

<body>

<div class="container">
    <form:form method="POST" modelAttribute="project" class="form-signin" action="projectCreate">
        <h2 class="form-signin-heading">Create your project</h2>
        <h3>Name</h3>
        <spring:bind path="name">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="name" class="form-control" placeholder="name"
                            autofocus="true"></form:input>
                <form:errors path="name"></form:errors>
            </div>
        </spring:bind>

        <h3>Description</h3>
        <spring:bind path="description">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input type="text" path="description" class="form-control" placeholder="description"></form:input>
                <form:errors path="description"></form:errors>
            </div>
        </spring:bind>

        <h3>Start Date</h3>
        <spring:bind path="dataStart">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input  type="date" path="dataStart" class="form-control" placeholder="dd-mm-yyyy"></form:input>
<%--                <form:input pattern = "yyyy-MM-dd" type="date" path="dataStart" class="form-control" placeholder="dataStart"></form:input>--%>
                <form:errors path="dataStart"></form:errors>
            </div>
        </spring:bind>

        <h3>Finish Date</h3>
        <spring:bind path="dataFinish">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:input  type="date" path="dataFinish" class="form-control" placeholder="dd-mm-yyyy" var="parsedDate"></form:input>
<%--                <form:input  type="date" path="dataFinish" class="form-control" placeholder="dd-mm-yyyy"></form:input>--%>
                <form:errors path="dataFinish"></form:errors>
            </div>
        </spring:bind>

        <h3>Status Project</h3>
        <spring:bind path="taskProjectStatus">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                        <form:select path="taskProjectStatus">
                        <form:options items="${types}" itemLabel="code"  />
                    </form:select>
            </div>
        </spring:bind>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
    </form:form>

</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="${contextPath}/resources/js/bootstrap.min.js"></script>
</body>
</html>