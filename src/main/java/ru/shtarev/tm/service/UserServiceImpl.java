package ru.shtarev.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ru.shtarev.tm.client.BackendClient;
import ru.shtarev.tm.entity.User;
import ru.shtarev.tm.entity.Users;
import ru.shtarev.tm.enums.UserRole;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    private final BackendClient backendClient;

    @Autowired
    public UserServiceImpl(BackendClient backendClient) {
        this.backendClient = backendClient;
    }

    @Override
    public User findUserByUsername(String name) {
        return backendClient.findUserByUsername(name);
    }

    @Override
    public Optional<User> saveUser(User user) {
        if (user.getPassword().toCharArray().length < 6) {
            return Optional.empty();
        }
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Set<UserRole> setRole = new HashSet<>();
        setRole.add(UserRole.REGULAR_USER);
        user.setUserRole(setRole);
        return Optional.of(backendClient.saveUser(user));
    }

    @Override
    public Users getAllUsers() {
        return backendClient.getAllUsers();
    }
}
