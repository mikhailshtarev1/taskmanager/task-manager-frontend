package ru.shtarev.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import ru.shtarev.tm.client.BackendClient;
import ru.shtarev.tm.entity.Project;
import ru.shtarev.tm.entity.ProjectDTO;
import ru.shtarev.tm.entity.User;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService {

    private final BackendClient backendClient;
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

    @Autowired
    public ProjectServiceImpl(BackendClient backendClient) {
        this.backendClient = backendClient;
    }


    @Override
    public Project findProjectByProjectId(String id) {
        return backendClient.findProjectByProjectId(id);
    }

    @Override
    public Project saveProject(ProjectDTO projectDTO) {
        Project project = new Project();
        Object princ = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = new User();
        if (princ instanceof UserDetails) {
            String userName = ((UserDetails) princ).getUsername();
            user = backendClient.findUserByUsername(userName);
        }
        if (user != null) {
            project.setUser(user);
        }
        project.setName(projectDTO.getName());
        project.setDescription(projectDTO.getDescription());
        project.setTaskProjectStatus(projectDTO.getTaskProjectStatus());
//        project.setDataStart(LocalDate.parse(projectDTO.getDataStart(), formatter));
//        project.setDataFinish(LocalDate.parse(projectDTO.getDataFinish(), formatter));
        project.setDataStart(LocalDateTime.of(LocalDate.parse(projectDTO.getDataStart(), formatter), LocalTime.now()));
        project.setDataFinish(LocalDateTime.of(LocalDate.parse(projectDTO.getDataFinish(), formatter), LocalTime.now()));
        return backendClient.saveProject(project);
    }

    @Override
    public List<Project> getAllProjects() {
        Object princ = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        String userName = ((UserDetails) princ).getUsername();
        return backendClient.getAllProjects(userName);
    }


}
