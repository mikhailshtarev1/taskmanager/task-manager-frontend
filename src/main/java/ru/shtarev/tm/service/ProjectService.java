package ru.shtarev.tm.service;

import ru.shtarev.tm.entity.Project;
import ru.shtarev.tm.entity.ProjectDTO;

import java.util.List;

public interface ProjectService {

    Project findProjectByProjectId(String id);

    Project saveProject(ProjectDTO project);

    List<Project> getAllProjects();
}
