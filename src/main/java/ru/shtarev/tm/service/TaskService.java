package ru.shtarev.tm.service;


import ru.shtarev.tm.entity.Task;

import java.util.List;

public interface TaskService {
    List<Task> findTasksByProject(String projectId);
}
