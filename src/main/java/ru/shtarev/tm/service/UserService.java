package ru.shtarev.tm.service;

import ru.shtarev.tm.client.BackendClient;
import ru.shtarev.tm.entity.User;
import ru.shtarev.tm.entity.Users;

import java.util.List;
import java.util.Optional;

public interface UserService {

    User findUserByUsername (String name);

    Optional<User> saveUser (User user);

    Users getAllUsers();
}
