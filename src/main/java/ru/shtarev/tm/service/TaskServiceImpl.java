package ru.shtarev.tm.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.shtarev.tm.client.BackendClient;
import ru.shtarev.tm.entity.Task;

import java.util.List;
@Service
public class TaskServiceImpl implements TaskService {
    private final BackendClient backendClient;

    @Autowired
    public TaskServiceImpl(BackendClient backendClient) {
        this.backendClient = backendClient;
    }

    @Override
    public List<Task> findTasksByProject(String projectId) {
        return backendClient.getAllTasksByProjectId(projectId);
    }
}
