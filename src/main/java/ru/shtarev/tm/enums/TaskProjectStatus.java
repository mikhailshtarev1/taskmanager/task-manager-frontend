package ru.shtarev.tm.enums;

public enum TaskProjectStatus {
    PLANNED("PLANNED"),
    IN_THE_PROCESS("IN_THE_PROCESS"),
    READY("READY");

    private String code;


    TaskProjectStatus(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }
}
