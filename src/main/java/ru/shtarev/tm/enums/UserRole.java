package ru.shtarev.tm.enums;

public enum UserRole {
    ADMIN,
    REGULAR_USER,
    PREMIUM_USER,
    NO_ROLE

}