package ru.shtarev.tm.client;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.util.UriComponentsBuilder;
import ru.shtarev.tm.entity.Project;

import javax.annotation.PostConstruct;
import java.net.URI;

@Configuration
public class UriConfig {
    private String scheme;
    private String host;
    private String port;

    private final UriComponentsBuilder builder = UriComponentsBuilder.newInstance();


    @PostConstruct
    private void init() {
        builder.scheme("http").host("localhost").port(8080);
    }

    public URI getUser(String name) {
        return builder.cloneBuilder()
                .path("/users/{name}")
                .build(name);

    }

    public URI getUsers() {
        return builder.cloneBuilder().path("/users").build().toUri();
    }

    public URI saveUser() {
        return builder.cloneBuilder().path("/users").build().toUri();
    }

    public URI getProject(String id) {
        return builder.cloneBuilder()
                .path("/projects/{id}")
                .build(id);
    }

    public URI getProjects(String userName){
        return builder
                .cloneBuilder()
                .path("/projects")
                .queryParam("userName",userName)
                .build().toUri();
    }
    public URI saveProject(){
        return builder.cloneBuilder().path("/projects").build().toUri();
    }

    public URI getTasksByProjectId(String projectId){
        return builder
                .cloneBuilder()
                .path("/tasks")
                .queryParam("projectId",projectId)
                .build().toUri();
    }

}
