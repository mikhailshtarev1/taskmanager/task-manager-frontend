package ru.shtarev.tm.client;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.istack.NotNull;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import ru.shtarev.tm.entity.*;

import java.util.Arrays;
import java.util.List;

@Component
public class BackendClient {

    private final RestTemplate restTemplate;
    private final UriConfig uriConfig;
    private final ObjectMapper objectMapper;


    public BackendClient(RestTemplate restTemplate, UriConfig uriConfig, ObjectMapper objectMapper) {
        this.restTemplate = restTemplate;
        this.uriConfig = uriConfig;
        this.objectMapper = objectMapper;
    }

    public User findUserByUsername(String name) {
        return restTemplate.getForObject(uriConfig.getUser(name), User.class);
    }

    public Users getAllUsers() {
        return restTemplate.getForObject(uriConfig.getUsers(), Users.class);
    }

    public User saveUser(User user) {
        return restTemplate.postForObject(uriConfig.saveUser(), user, User.class);
    }


    public Project findProjectByProjectId(@NotNull final String id) {
        return restTemplate.getForObject(uriConfig.getProject(id), Project.class);
    }

    ;

    public List<Project> getAllProjects(String userName) {
        return Arrays.asList(restTemplate.getForObject(uriConfig.getProjects(userName), Project[].class));
    }

    public Project saveProject(Project project) {
        return restTemplate.postForObject(uriConfig.saveProject(), project, Project.class);
    }

    public List<Task> getAllTasksByProjectId (String projectId){
        return Arrays.asList(restTemplate.getForObject(uriConfig.getTasksByProjectId(projectId), Task[].class));
    }


}
