package ru.shtarev.tm.configs;

import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.shtarev.tm.entity.User;
import ru.shtarev.tm.enums.UserRole;
import ru.shtarev.tm.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service(value = "userDetailServiceImpl")
public class UserDetailsServiceImpl implements UserDetailsService {

    private final UserService userService;

    @Autowired
    public UserDetailsServiceImpl(UserService userService) {
        this.userService = userService;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(@NotNull final String username) throws UsernameNotFoundException {
        final User user = findByUsername(username);
        if (user == null) throw new UsernameNotFoundException("Пользователь не найден");
        org.springframework.security.core.userdetails.User.UserBuilder builder = null;
        builder = org.springframework.security.core.userdetails.User.withUsername(username);
        builder.password(user.getPassword());
        final Set<UserRole> userRoles = user.getUserRole();
        final List<String> roles = new ArrayList<>();
        for (UserRole role : userRoles) roles.add(role.toString());
        builder.roles(roles.toArray(new String[]{}));
        return builder.build();
    }

    private User findByUsername(@NotNull final String username) {
        if (username == null || username.isEmpty()) return null;
        return userService.findUserByUsername(username);
    }
}
