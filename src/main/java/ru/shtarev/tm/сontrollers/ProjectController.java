package ru.shtarev.tm.сontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.shtarev.tm.entity.Project;
import ru.shtarev.tm.entity.ProjectDTO;
import ru.shtarev.tm.entity.Task;
import ru.shtarev.tm.enums.TaskProjectStatus;
import ru.shtarev.tm.service.ProjectService;
import ru.shtarev.tm.service.TaskService;

import java.util.List;

@Controller
public class ProjectController {

    private final ProjectService projectService;
    private final TaskService taskService;

   @Autowired
    public ProjectController(ProjectService projectService, TaskService taskService) {
        this.projectService = projectService;
        this.taskService = taskService;
    }


    @GetMapping("/projectCreate")
    public String projectCreate(Model model) {
        model.addAttribute("project", new ProjectDTO());
        model.addAttribute("types", TaskProjectStatus.values());
        return "projectCreate";
    }

    @PostMapping("/projectCreate")
    public String projectCreate(@ModelAttribute("project") ProjectDTO projectForm, BindingResult bindingResult) {
        projectService.saveProject(projectForm);
        return "redirect:/usermain";
    }

    @GetMapping({"/projectList"})
    public String projectList(Model model) {
        List<Project> projectList = projectService.getAllProjects();
        model.addAttribute("projects", projectList);
        return "projectList";
    }

    @GetMapping({"/project/{id}"})
    public String getProject(@PathVariable(value = "id") final String id, Model model) {
        Project project = projectService.findProjectByProjectId(id);
        List<Task> taskList = taskService.findTasksByProject(id);
        model.addAttribute("projects", project);
        model.addAttribute("taskLists", taskList);
        return "getProject";
    }


}
