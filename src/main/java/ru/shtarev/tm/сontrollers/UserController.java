package ru.shtarev.tm.сontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.shtarev.tm.entity.User;
import ru.shtarev.tm.entity.Users;
import ru.shtarev.tm.service.UserService;

@Controller
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/usermain")
    public String userMain(Model model) {
        return "usermain";
    }

    @GetMapping("/")
    public String showPTMenu(ModelMap model) {
        return "userList";
    }

    @GetMapping({"/userList"})
    public String userList(Model model) {
        Users listUser = userService.getAllUsers();
        model.addAttribute("users", listUser.getListUser());
        return "userList";
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("users", new User());
        return "registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("users") User userForm, BindingResult bindingResult) {
        userService.saveUser(userForm);
        return "redirect:/usermain";
    }


}
