package ru.shtarev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import ru.shtarev.tm.enums.TaskProjectStatus;

@Getter
@Setter
public class ProjectDTO {

    private String name;

    private String description;

    private String dataStart;

    private String dataFinish;

    private TaskProjectStatus taskProjectStatus;
}
