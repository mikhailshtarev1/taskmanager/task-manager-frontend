package ru.shtarev.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.validation.annotation.Validated;
import ru.shtarev.tm.enums.UserRole;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Set;
import java.util.UUID;


@Getter
@Setter
@Validated
public class User {

    @Size(min = 6, max = 255)
    private String name;

    @NotNull
    @Size(min = 6, max = 255)
    private String password;


    private Set<UserRole> userRole;


    private String userId = UUID.randomUUID().toString();

}


