package ru.shtarev.tm.entity;

import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
public class Tasks {

    private List<Task> listTask;
}
